// Initialize your app
var myApp = new Framework7({
    // remove fast click, this might case issue on iframe
    // fastClicks: false
});

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});


(function() {
    // NOTE: some static url file only, miltan will give api when it's ready
    var url = 'http://staging.dentaltrax.com/assets/tmp/api/pdf_link/';

    var isRequesting = false;
    // fetch for pdf link
    var fetchPDFLink = function() {
        // exit immediately if it's still requesting
        if( isRequesting )
            return;

        isRequesting = true;

        $.ajax({
            url: url,
            dataType: 'json',
            success: function(response) {
                isRequesting = false;

                // check if there's pdf stored
                if( response.pdf_link ) {
                    // send to the server to reset the link immediately
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        type: 'post',
                        data: {
                            // this will reset the pdf
                            pdf_link: ''
                        },
                        success: function() {
                            alert('PDF_RESET_SUCCESS');

                            // NOTE: open the inappbrowser here, note that it will
                            // stop fetching when inappbrowser is open, be sure to call
                            // 'fetchPDFLink' again when the inappbrowser closes
                            //alert(response.pdf_link);
                            var ref = cordova.InAppBrowser.open(response.pdf_link, '_blank', 'hidden=no');
//                            // some time later...
                            ref.show();
                            fetchPDFLink();
                        },
                        error: function() {
                            alert('PDF_RESET_FAIL');
                        }
                    });

                // if there's still no pdf stored, fetch again
                }else {
                    fetchPDFLink();
                }
            },
            error: function() {
                isRequesting = false;
                // fetch again for pdf link when it closes
                fetchPDFLink();
            }
        });
    };
    // fetch the pdf on init
    //fetchPDFLink();
})();
